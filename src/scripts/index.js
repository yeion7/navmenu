require('babel-core/register');
require('babel-polyfill');

import '../styles/main.css';
import {
  getAPI,
  getMenuItems,
  buildMenuDom,
  handleToggleMenu,
  onClickMask,
  closeAll,
} from '../lib';

// Instance the endpoint
const ENDPOINT = '/api/nav.json';
const API = getAPI(ENDPOINT);

/**
 * Call the endpoint for items and build the menu, and append to root
 * @param  {Element}  root Element to append menu
 * @return {Promise}      [description]
 */

const appendMenu = async root => {
  const items = await API.getMenuItems();
  const $menu = buildMenuDom(items);
  root.appendChild($menu);
};

document.addEventListener('DOMContentLoaded', function() {
  const $nav = document.getElementById('nav');
  const $menuButton = document.getElementById('menu-button');
  const $mask = document.getElementById('mask');

  appendMenu($nav);
  $menuButton.addEventListener('click', handleToggleMenu);
  $mask.addEventListener('click', onClickMask);
  window.onhashchange = closeAll;
});
