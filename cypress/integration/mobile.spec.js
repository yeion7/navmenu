describe('test mobile menu', () => {
  beforeEach(function() {
    cy.visit('/');
    cy.viewport('iphone-6');
  });

  it('Hamburger icon will display in the top-left of the page', () => {
    cy.get('#menu-button').should('be.visible');
    cy.get('.header__logo').should('not.be.visible');
    cy.get('.mask').should('not.be.visible');
    cy.get('.header').should('not.have.class', 'open');

    cy.wait(1000);
    cy.screenshot('close_mobile', { log: false });
  });

  it('When a user clicks the open navigation icon (“hamburger”), the navigation should “push” from left to right', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy
      .get('.header__nav > ul')
      .should('have.class', 'menu-open')
      .and('have.css', 'left')
      .and('match', /0px/);

    cy.screenshot('open_mobile', { log: false });
  });

  it('The HUGE logo and navigation toggle slide left to right', () => {
    cy.get('.header__logo').should('not.be.visible');

    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header__logo').should('be.visible');
  });

  it('The open navigation icon should change to the close navigation icon (“x”)', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .and('have.css', 'background-image')
      .and('match', /open.svg/);

    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true')
      .and('have.css', 'background-image')
      .and('match', /close.svg/);

    cy.get('.header').should('have.class', 'open');
  });

  it('Translucent mask appears over content, right of navigation', () => {
    cy.get('#mask').should('not.be.visible');

    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy.get('#mask').should('be.visible');
  });

  it('When a user clicks a Primary Navigation link item, the browser should navigate to a new page', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy
      .get('.header__nav > ul > li > a')
      .filter('a[href="#/news"]')
      .should('have.attr', 'href', '#/news')
      .click()
      .url()
      .should('include', '/news');
  });

  it('When a user clicks a Primary Navigation menu item, the Secondary Navigation should “push” down, the chevron should rotate * 180°.', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy
      .get('.header__nav > ul > li')
      .filter('li[role="menuitem"]')
      .eq(1)
      .click()
      .should('have.class', 'open');
  });

  it('When a user hovers a Secondary Navigation item, it should change color (magenta/light gray)', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy
      .get('.header__nav > ul > li')
      .filter('li[role="menuitem"]')
      .eq(1)
      .should('be.visible');

    // TODO: Implement .hover when cypress api is available
  });

  it('When a user clicks a Secondary Navigation item, browser should navigate to a new page', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy
      .get('li[role="menuitem"] > ul > li > a')
      .eq(1)
      .should('have.attr', 'href', '#/about/how-we-work')
      .click({ force: true })
      .url()
      .should('include', 'work');
  });

  it('When a user clicks outside of the navigation, the navigation should close.', () => {
    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true');

    cy.get('.header').should('have.class', 'open');

    cy.get('#mask').click({ force: true });

    cy.get('.header').should('not.have.class', 'open');

    cy
      .get('.header__nav > ul')
      .should('have.class', 'menu-open')
      .and('have.css', 'left')
      .and('match', /0px/);

    cy
      .get('#menu-button')
      .should('have.class', 'header__button')
      .and('have.css', 'background-image')
      .and('match', /open.svg/);
  });
});
