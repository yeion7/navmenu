import {
  getParent,
  getTarget,
  getHeader,
  toggleExpanded,
  toggleOpenClass,
  maskToggle,
  closeAllSubmenus,
  createLink,
  createItem,
  createMenuItem,
} from '../index.js';

describe('all lib function works', () => {
  test('can get parent to element', () => {
    document.body.innerHTML = `
    <div id='parent'>
      <span id="child">target</span>
    </div>
    `;

    const child = document.getElementById('child');
    const parent = document.getElementById('parent');

    child.addEventListener('click', e => {
      expect(getParent(e)).toEqual(e.target.parentNode);
    });

    child.click();
  });

  test('can get target to event ', () => {
    document.body.innerHTML = `
    <div id='parent'>
      <span id="child">target</span>
    </div>
    `;

    const child = document.getElementById('child');

    child.addEventListener('click', e => {
      expect(getTarget(e)).toEqual(e.target);
    });

    child.click();
  });

  test('can get element with id header', () => {
    document.body.innerHTML = `
    <header id='header'>
    </header>
    `;

    const $header = document.getElementById('header');

    expect(getHeader()).toEqual($header);
  });

  test('can toggle aria expanded attribute', () => {
    document.body.innerHTML = `
    <li id='item' aria-expanded="false">
      <a id='link' href="">Link</a>
    </li>
    `;

    const $item = document.getElementById('item');

    expect(toggleExpanded($item)).toMatchSnapshot();
  });

  test('can toggle open class', () => {
    document.body.innerHTML = `
    <li id='item' class="">
      <a id='link' href="">Link</a>
    </li>
    `;

    const $item = document.getElementById('item');

    expect(toggleOpenClass($item)).toMatchSnapshot();
  });

  test('can toggle open class on mask element', () => {
    document.body.innerHTML = `
    <div id='mask' class="">
      <a id='link' href="">Link</a>
    </div>
    `;

    const $mask = document.getElementById('mask');

    expect(toggleOpenClass($mask)).toMatchSnapshot();
  });

  test('toggle all data attributes aria-expanded, except the recived', () => {
    document.body.innerHTML = `
    <ul>
      <li id="item" aria-expanded="true"><a href="">link</a></li>
      <li aria-expanded="true"><a href="">link</a></li>
      <li aria-expanded="true"><a href="">link</a></li>
    </ul>
    `;

    const $item = document.getElementById('item');

    expect(closeAllSubmenus($item)).toMatchSnapshot();
  });

  test('Can create a link element', () => {
    const node = {
      url: '/',
      label: 'link',
      items: [],
    };

    expect(createLink(node)).toMatchSnapshot();
  });

  test('Can create a link element with role button', () => {
    const node = {
      url: '/',
      label: 'link',
      items: [
        {
          url: '/help',
          label: 'submenu',
        },
      ],
    };

    expect(createLink(node)).toMatchSnapshot();
  });

  test('Can create a item element', () => {
    const node = {
      url: '/',
      label: 'link',
      items: [],
    };

    expect(createItem(createLink(node))).toMatchSnapshot();
  });

  test('Can create a item element with submenu', () => {
    const node = {
      url: '/',
      label: 'link',
      items: [
        {
          url: '/help',
          label: 'submenu',
        },
        {
          url: '/os',
          label: 'os',
        },
      ],
    };

    expect(createItem(createLink(node))).toMatchSnapshot();
  });

  test('Can create a menu element with submenu', () => {
    const node = {
      url: '/',
      label: 'link',
      items: [
        {
          url: '/help',
          label: 'submenu',
        },
        {
          url: '/os',
          label: 'os',
        },
      ],
    };

    const $root = document.createElement('ul');

    expect(createMenuItem($root)($root, node)).toMatchSnapshot();
  });
});
