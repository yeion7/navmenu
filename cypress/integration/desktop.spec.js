describe('test desktop menu', () => {
  beforeEach(function() {
    cy.visit('/');
    cy.viewport('macbook-13');
  });

  it('The nav will display as a horizontal nav, No hamburger will be shown', () => {
    cy.get('#menu-button').should('not.be.visible');
    cy.get('#mask').should('not.be.visible');
    cy.get('.header').should('not.have.class', 'open');

    cy
      .get('.header__nav > ul')
      .should('be.visible')
      .and('have.css', 'position')
      .and('match', /relative/);

    cy.screenshot('close_desktop', { log: false });
  });

  it('On click, if item contains a URL, Primary Navigation navigates to a new page', () => {
    cy
      .get('.header__nav > ul > li > a')
      .filter('a[href="#/work"]')
      .should('have.attr', 'href', '#/work')
      .click()
      .url()
      .should('include', 'work');
  });

  it('On click, if item contains other items, Secondary Navigation appears', () => {
    cy
      .get('.header__nav > ul > li')
      .filter('li[role="menuitem"]')
      .eq(1)
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true')
      .should('have.class', 'open');

    cy.get('li[role="menuitem"].open > ul').should('be.visible');
  });

  it('On click, if item contains other items, Secondary Navigation appears', () => {
    cy
      .get('.header__nav > ul > li')
      .filter('li[role="menuitem"]')
      .eq(1)
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true')
      .should('have.class', 'open');

    cy.screenshot('open_desktop', { log: false });

    cy
      .get('#mask')
      .should('be.visible')
      .should('have.class', 'open');
  });

  it('On click, if item contains other items, Secondary Navigation appears', () => {
    cy
      .get('.header__nav > ul > li')
      .filter('li[role="menuitem"]')
      .eq(1)
      .should('have.attr', 'aria-expanded', 'false')
      .click()
      .should('have.attr', 'aria-expanded', 'true')
      .should('have.class', 'open');

    cy
      .get('#mask')
      .click({ force: true })
      .should('not.be.visible')
      .should('not.have.class', 'open');
  });
});
