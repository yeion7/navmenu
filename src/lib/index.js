/**
 * Helper compose functions, recive functions and return a function
 * @param  {Function} funcs recive N functions
 * @return {Function}       Compose all function recived
 */
export function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg;
  }

  if (funcs.length === 1) {
    return funcs[0];
  }

  return funcs.reduce((a, b) => (...args) => a(b(...args)));
}

/**
 * get the parent node of element clicked
 * @param  {Event} e Event HTML
 * @return {Element}   Element HTML
 */
export const getParent = e => e.target.parentNode;

/**
 * get the target node of element clicked
 * @param  {Event} e Event HTML
 * @return {Element}   Element HTML
 */
export const getTarget = e => e.target;

/**
 * return the header element
 * @param  {void}
 * @return {Element}   Element HTML
 */
export const getHeader = () => document.getElementById('header');

/**
 * Change the aria-expanded attribute and return the same element
 * @param  {Element} $element Element HTML
 * @return {Element}   Element HTML
 */
export const toggleExpanded = $element => {
  const isExpanded =
    $element.getAttribute('aria-expanded') === 'true' ? true : false;

  $element.setAttribute('aria-expanded', !isExpanded);

  return $element;
};

/**
 * toggle the class open of recived element
 * @param  {Element} $element HTML ELEMENT
 * @return {Element}          HTML ELEMENT
 */

export const toggleOpenClass = $element => {
  $element.classList.toggle('open');

  return $element;
};

/**
 * Create a new function, toggle class to mask element
 * @type {Function}
 */

export const maskToggle = toggleOpenClass.bind(
  null,
  document.getElementById('mask')
);

/**
 * Find all li with aria-expanded attribute on true and set in false
 * @param  {Element} $element HTML ELEMENT
 * @return {Element}          HTML ELEMENT
 */

export const closeAllSubmenus = $element => {
  const $menuItemExpanded = document.querySelectorAll(
    'li[aria-expanded="true"]'
  );

  Array.from($menuItemExpanded).forEach(item => {
    if (item !== $element) {
      item.setAttribute('aria-expanded', 'false');

      if (item.classList.contains('open')) {
        item.classList.remove('open');
      }
    }
  });

  return $element;
};

const openMask = $element => {
  const $mask = document.getElementById('mask');

  if (!$mask.classList.contains('open')) {
    $mask.classList.add('open');
  }

  return $element;
};

const closeMask = $element => {
  const $mask = document.getElementById('mask');

  if ($mask.classList.contains('open')) {
    $mask.classList.remove('open');
  }

  return $element;
};

export const toggleSubmenuItems = compose(
  openMask,
  toggleOpenClass,
  toggleExpanded,
  closeAllSubmenus,
  getParent
);

export const onClickMask = compose(
  closeAllSubmenus,
  toggleOpenClass,
  getHeader,
  toggleOpenClass,
  getTarget
);

export const handleToggleMenu = compose(
  maskToggle,
  toggleOpenClass,
  getHeader,
  toggleExpanded,
  getTarget
);

export const closeAll = compose(
  closeMask,
  closeAllSubmenus,
  toggleOpenClass,
  getHeader
);

/**
 * Create a new a element with atributes
 * @param  {string} url   text of link
 * @param  {string} label href
 * @param  {Array} items Subitems
 * @return {Element}       a Element
 */

export const createLink = ({ url, label, items } = {}) => {
  const a = document.createElement('a');
  a.href = url;
  a.innerText = label;
  a.tabIndex = 0;

  if (items && items.length > 0) {
    a.setAttribute('role', 'button');
    a.removeAttribute('href');
  }

  return a;
};

/**
 * Create a li element, recived a element (a), and wrap that element
 * @param  {Element} element a element
 * @return {Element}         li element
 */

export const createItem = element => {
  const li = document.createElement('li');
  li.setAttribute('role', 'link');
  li.appendChild(element);

  if (element.hasAttribute('role')) {
    li.setAttribute('aria-haspopup', true);
    li.setAttribute('aria-expanded', false);
    li.setAttribute('role', 'menuitem');
    li.onclick = toggleSubmenuItems;
  }
  return li;
};

/**
 * Reduce function, recive a root element (ul) and return the reduce function for create a menu
 * @param  {Element} root HTML ELEMENT
 * @return {Element}      Menu Item
 */

export const createMenuItem = root => (
  menu = document.createElement('ul'),
  node = { label: '', url: '', items: [] }
) => {
  menu.setAttribute('role', 'menu');
  menu.setAttribute('aria-label', 'main manu');
  menu.className = 'animation menu-size menu-close menu-open';

  const li = createItem(createLink(node));

  if (node.items && node.items.length > 0) {
    li.appendChild(buildMenuDom(node.items, root));
  }

  menu.appendChild(li);

  return menu;
};

/**
 * Itera over a array and build the dom for menu
 * @param  {Array}  [elements=[]]                       list items for menu
 * @param  {Element} [root=document.createElement('ul')] container for menu and submenus
 * @return {Element}                                     HTML ELEMENT
 */
export const buildMenuDom = (
  elements = [],
  root = document.createElement('ul')
) => elements.reduce(createMenuItem(root), root.cloneNode(true));

/**
 * Return a object with api methods
 * @param  {string]} endpoint Endpoint url
 * @return {Object}          object api
 */

export const getAPI = endpoint => ({
  getMenuItems: async () => {
    const fallback = [
      {
        label: 'Help',
        url: '#/help',
      },
    ];

    try {
      const response = await fetch(endpoint);

      if (response.status === 200) {
        const data = await response.json();
        return data.items;
      } else {
        return fallback;
      }
    } catch (e) {
      console.error(e);
      return fallback;
    }
  },
});
