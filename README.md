# Huge Navigation Exercise

## Implementation details

### Scrips

* `npm run dev`: serve project on port _9000_ using webpack for building process
* `npm run build`: build the project and save result on `public` folder
* `npm run test:cypress`: run cypres with interface
* `npm run test:watch`: run jest for unit test with watch mode
* `npm run test`: run unit test
* `npm run test:e2e`: try to run server and run e2e test on terminal mode
* `npm run start`: build the project and start server

## Testing

### Unit Test

Unit test use jest and test all utils functions

### E2E test

E2E test use Cypress and require server up, algo require config the url in the
file `.cypress.json`

### Test cases :

#### Desktop

* [x] On hover, Primary Navigation reverses color (white/magenta).
* [x] On click, if item contains a URL, Primary Navigation navigates to a new
      page.
* [x] On click, if item contains other items, Secondary Navigation appears (see
      Desktop, Secondary Navigation).
* [x] Menu appears containing Secondary Navigation.
* [x] Translucent mask appears over content, behind menu.
* [x] On hover in, Secondary Navigation changes color (magenta/light gray).
* [x] On click, Secondary navigates to a new page.
* [x] On click outside of menu, menu and mask are hidden.

#### Mobile

* [x] When a user clicks the open navigation icon (“hamburger”), the navigation
      should “push” from left to right.
* [x] The HUGE logo and navigation toggle slide left to right.
* [x] The open navigation icon should change to the close navigation icon (“x”).
* [x] Translucent mask appears over content, right of navigation.
* [x] The Primary Navigation should include link items and menu items.
* [x] When a user hovers a Primary Navigation item, it should change color
      (magenta/light gray).
* [x] When a user clicks a Primary Navigation link item, the browser should
      navigate to a new page.
* [x] When a user clicks a Primary Navigation menu item, the Secondary
      Navigation should “push” down, the chevron should rotate \* 180°.
* [x] When a user hovers a Secondary Navigation item, it should change color
      (magenta/light gray).
* [x] When a user clicks a Secondary Navigation item, browser should navigate to
      a new page.
* [x] When a user clicks outside of the navigation, the navigation should close.
* [x] When the navigation closes: _ \* the menu should “pull” from right to left
      _ _ the logo and toggle button should “slide” from right to left \_ _ the
      close icon should change to the open icon \_ [ ] the mask should be hidden
